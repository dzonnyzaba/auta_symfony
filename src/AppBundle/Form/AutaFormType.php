<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class AutaFormType extends AbstractType{
    
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
                //->add('id')
                ->add('marka')
                ->add('model')
                ->add('lata_produkcji')
                ->add('szerokosc')
                ->add('dlugosc')
                ->add('waga')
                ->add('factorcx',TextType::class, array('label' => 'Współczynnik Cx'))
                ->add('zawieszenie')
                ->add('zalety')
                ->add('wady')
                ->add('silnik1', TextareaType::class)
                ->add('silnik2', TextareaType::class)
                ->add('silnik3', TextareaType::class)
                ->add('silnik4', TextareaType::class)
                ->add('silnik5', TextareaType::class);
    }
    
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Auta'
        ]);
    }
}