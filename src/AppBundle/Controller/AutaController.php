<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Form\AutaFormType;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Auta;

class AutaController extends Controller{
    
    /**
     * @Route("/auta", name="_home")
     */
    public function autaAction(){
        $em = $this->getDoctrine()->getManager();
        $auta = $em->getRepository('AppBundle\Entity\Auta')->findAll();
        
        return $this->render('default/home.html.twig', array('auta' => $auta));
    }
    
    /**
     * 
     * @Route("auta/admin/dodaj_auto", name="dodaj_auto")
     */
    public function dodajAction(Request $request){
        
        $form = $this->createForm(AutaFormType::class);
        
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $auto = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($auto);
            $em->flush();
            
            return $this->redirectToRoute('_home');
        }
        return $this->render('default/nowy.html.twig', 
                array('autaForm' => $form->createView()));
    }
    
    /**
     * @Route("/auta/admin/{id}/edit", name="auta_edit")
     */
    public function editAction(Request $request, Auta $auta){
        $form = $this->createForm(AutaFormType::class, $auta);
        
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $auto = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($auto);
            $em->flush();
            
            $this->addFlash('success', 'Zmodyfikowano dane');
            
            return $this->redirectToRoute('_home');
        }
        return $this->render('default/edit.html.twig',
                ['edytuj_form' => $form->createView() ]);
    }
    
    /**
     * @Route("/auta/admin/{id}/usun", name="auta_usun")
     */
    public function usunAction(Request $request, Auta $auta, $id){
        $form = $this->createForm(AutaFormType::class, $auta);
        
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            
            $em = $this->getDoctrine()->getManager();
            $auto = $em->getRepository('AppBundle:Auta')->find($id);
            
            $em->remove($auto);
            $em->flush();
            
            return $this->redirectToRoute('_home');
        }
        return $this->render('default/usun.html.twig',
                ['usun_form' => $form->createView(),
                    'ajdi' => $id ]);
    }
    
    /**
     * @Route("/auta/porownanie/{auto1}/{auto2}", defaults={"auto1"=0, "auto2"=0}, name="auta_porownywarka")
     */
    public function porownajAction($auto1, $auto2){
        
        $em = $this->getDoctrine()->getManager();
        $auta = $em->getRepository('AppBundle\Entity\Auta')->findById(
                [$auto1, $auto2]
                );
        
        return $this->render('default/porownanie.html.twig', [
                'auta' => $auta
        ]
        );
    }
}