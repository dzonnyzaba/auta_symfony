<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="`auta`")
 */
class Auta{
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string")
     */
    private $Marka;
    
        /**
     * @ORM\Column(type="string")
     */
    private $Model;
    
        /**
     * @ORM\Column(type="string")
     */
    private $Lata_produkcji;
    
        /**
     * @ORM\Column(type="string")
     */
    private $Szerokosc;
    
        /**
     * @ORM\Column(type="string")
     */
    private $Dlugosc;
    
    /**
     *
     * @ORM\Column(type="string")
     */
    private $Waga;
    
    /**
     * @ORM\Column(type="string")
     */
    private $factorcx;
            
    /**
     * @ORM\Column(type="string")
     */
    private $Zawieszenie;
    
        /**
     * @ORM\Column(type="string")
     */
    private $Zalety;
    
        /**
     * @ORM\Column(type="string")
     */
    private $Wady;
    
        /**
     * @ORM\Column(type="text")
     */
    private $Silnik1;
    
     /**
     * @ORM\Column(type="text")
     */
    private $Silnik2;
    
     /**
     * @ORM\Column(type="text")
     */
    private $Silnik3;
    
     /**
     * @ORM\Column(type="text")
     */
    private $Silnik4;
    
     /**
     * @ORM\Column(type="text")
     */
    private $Silnik5;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marka
     *
     * @param string $marka
     *
     * @return Auta
     */
    public function setMarka($marka)
    {
        $this->Marka = $marka;

        return $this;
    }

    /**
     * Get marka
     *
     * @return string
     */
    public function getMarka()
    {
        return $this->Marka;
    }

    /**
     * Set model
     *
     * @param string $model
     *
     * @return Auta
     */
    public function setModel($model)
    {
        $this->Model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string
     */
    public function getModel()
    {
        return $this->Model;
    }

    /**
     * Set lataProdukcji
     *
     * @param string $lataProdukcji
     *
     * @return Auta
     */
    public function setLataProdukcji($lataProdukcji)
    {
        $this->Lata_produkcji = $lataProdukcji;

        return $this;
    }

    /**
     * Get lataProdukcji
     *
     * @return string
     */
    public function getLataProdukcji()
    {
        return $this->Lata_produkcji;
    }

    /**
     * Set szerokosc
     *
     * @param string $szerokosc
     *
     * @return Auta
     */
    public function setSzerokosc($szerokosc)
    {
        $this->Szerokosc = $szerokosc;

        return $this;
    }

    /**
     * Get szerokosc
     *
     * @return string
     */
    public function getSzerokosc()
    {
        return $this->Szerokosc;
    }

    /**
     * Set dlugosc
     *
     * @param string $dlugosc
     *
     * @return Auta
     */
    public function setDlugosc($dlugosc)
    {
        $this->Dlugosc = $dlugosc;

        return $this;
    }

    /**
     * Get dlugosc
     *
     * @return string
     */
    public function getDlugosc()
    {
        return $this->Dlugosc;
    }

    /**
     * Set zawieszenie
     *
     * @param string $zawieszenie
     *
     * @return Auta
     */
    public function setZawieszenie($zawieszenie)
    {
        $this->Zawieszenie = $zawieszenie;

        return $this;
    }

    /**
     * Get zawieszenie
     *
     * @return string
     */
    public function getZawieszenie()
    {
        return $this->Zawieszenie;
    }

    /**
     * Set zalety
     *
     * @param string $zalety
     *
     * @return Auta
     */
    public function setZalety($zalety)
    {
        $this->Zalety = $zalety;

        return $this;
    }

    /**
     * Get zalety
     *
     * @return string
     */
    public function getZalety()
    {
        return $this->Zalety;
    }

    /**
     * Set wady
     *
     * @param string $wady
     *
     * @return Auta
     */
    public function setWady($wady)
    {
        $this->Wady = $wady;

        return $this;
    }

    /**
     * Get wady
     *
     * @return string
     */
    public function getWady()
    {
        return $this->Wady;
    }

    /**
     * Set silnik1
     *
     * @param string $silnik1
     *
     * @return Auta
     */
    public function setSilnik1($silnik1)
    {
        $this->Silnik1 = $silnik1;

        return $this;
    }

    /**
     * Get silnik1
     *
     * @return string
     */
    public function getSilnik1()
    {
        return $this->Silnik1;
    }

    /**
     * Set silnik2
     *
     * @param string $silnik2
     *
     * @return Auta
     */
    public function setSilnik2($silnik2)
    {
        $this->Silnik2 = $silnik2;

        return $this;
    }

    /**
     * Get silnik2
     *
     * @return string
     */
    public function getSilnik2()
    {
        return $this->Silnik2;
    }

    /**
     * Set silnik3
     *
     * @param string $silnik3
     *
     * @return Auta
     */
    public function setSilnik3($silnik3)
    {
        $this->Silnik3 = $silnik3;

        return $this;
    }

    /**
     * Get silnik3
     *
     * @return string
     */
    public function getSilnik3()
    {
        return $this->Silnik3;
    }

    /**
     * Set silnik4
     *
     * @param string $silnik4
     *
     * @return Auta
     */
    public function setSilnik4($silnik4)
    {
        $this->Silnik4 = $silnik4;

        return $this;
    }

    /**
     * Get silnik4
     *
     * @return string
     */
    public function getSilnik4()
    {
        return $this->Silnik4;
    }

    /**
     * Set silnik5
     *
     * @param string $silnik5
     *
     * @return Auta
     */
    public function setSilnik5($silnik5)
    {
        $this->Silnik5 = $silnik5;

        return $this;
    }

    /**
     * Get silnik5
     *
     * @return string
     */
    public function getSilnik5()
    {
        return $this->Silnik5;
    }


    /**
     * Set waga
     *
     * @param string $waga
     *
     * @return Auta
     */
    public function setWaga($waga)
    {
        $this->Waga = $waga;

        return $this;
    }

    /**
     * Get waga
     *
     * @return string
     */
    public function getWaga()
    {
        return $this->Waga;
    }

    /**
     * Set factorcx
     *
     * @param string $factorcx
     *
     * @return Auta
     */
    public function setFactorcx($factorcx)
    {
        $this->factorcx = $factorcx;

        return $this;
    }

    /**
     * Get factorcx
     *
     * @return string
     */
    public function getFactorcx()
    {
        return $this->factorcx;
    }
}
